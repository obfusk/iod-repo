* [ ] The app complies with the inclusion criteria as listed [in the README](https://gitlab.com/IzzyOnDroid/repo/blob/master/README.md#what-are-the-requirements-an-app-must-meet-to-be-included-with-the-repo)
* [ ] The app is not already listed in the repo or issue tracker

---------------------

### Link to the source code:
### Link to app in another app store:
### License used:
### Category:
### Summary:
### Description:
