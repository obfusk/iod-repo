## Assets
Here you find some assets associated with *IzzyOnDroid*, which can be used freely under the conditions outlined below.

## Licenses
The 3D Android [is licensed to the public domain](https://www.clker.com/disclaimer.html). You can find it [here](https://www.clker.com/clipart-188250.html).

The background of the IzzyOnDroid logo is a registered trademark by [IzzySoft](https://www.izzysoft.de/).

## Usage terms aka The Legalese
Use of the corresponding artwork presented here is allowed only in association with IzzyOnDroid.
The reason for this restriction is that contains the IzzySoft logo (stylized letters "i" and "S"), which is
trademarked (German: Wort-Bild-Marke, in the register of the [DPMA](https://www.dpma.de/)).
Wherever one of these graphics containing the trademark is used,

* the graphic must be used unaltered, as provided. You may of course resize it to fit your content.
* you are permitted to match the badge language to the language of the place you're presenting it. The spelling of term *IzzyOnDroid* must be kept as-is.
* any online use of the badge must link to content on the corresponding IzzyOnDroid websites.[^1]
* the graphics must not be used to promote content other than that at the mentioned IzzyOnDroid websites.
* you should include the appropriate legal attribution when there is space in the creative.

Note that any use of the trademarked IzzySoft graphic itself outside of this collection is not covered by above grant.
The grant outlined only covers the graphics presented here.

## Graphics
| Graphic | Description |
| ------- | ----------- |
| ![Original Badge](IzzyOnDroid.png){width=250} | The original badge with a white boarder and transparent spacing around it<br>[PNG](IzzyOnDroid.png) |
| ![Original Badge NoFrame](IzzyOnDroid2.png){width=200} | Same badge without the border and without transparent spacing around<br>[PNG](IzzyOnDroid2.png) |
| ![Badge by Wolfshappen](IzzyOnDroidButton.png){width=200} | Badge reworked by Wolfshappen<br>[Noto SVG](IzzyOnDroidButton.svg)[^2] \| [NoFont SVG](IzzyOnDroidButton_nofont.svg) \| [PNG](IzzyOnDroidButton.png) |
| ![IzzyOnDroid Logo](IzzyOnDroidLogo.png){width=50} | IzzyOnDroid Logo reworked by Wolfshappen, as used in the badges<br>[SVG](IzzyOnDroidLogo.svg) \| [PNG](IzzyOnDroidLogo.png) |

## Credits
Credits to *Wolfshappen from Telegram* for re-creating and polishing the graphics in [SVG](https://en.wikipedia.org/wiki/SVG) format and thus allowing for much cleaner variants in different sizes!


## Footnotes
[^1]: as of this writing in 12/2023, these are: [android.izzysoft.de](https://android.izzysoft.de/) and [apt.izzysoft.de](https://apt.izzysoft.de/fdroid).
[^2]: requires the open-source *Noto Sans* font.
