#!/usr/bin/php
<?php
# Collect library statistics and output them as Markdown

function logger($msg) {
  echo "${msg}\n";
  $GLOBALS['prot'][] = $msg;
}

/*
  # point out missing descriptions in libinfo.jsonl
  [[ -n "$(jq -r .id ./libsmali.jsonl | grep -v "$(jq -r .id ./libinfo.jsonl)")" ]] && { #"
    echo "These libraries lack entries in libinfo.jsonl:"
    jq -r .id ./libsmali.jsonl | grep -v "$(jq -r .id ./libinfo.jsonl)"
    #RC+=1
  }
*/

$libpath  = dirname(dirname(__FILE__)).'/lib';
$libsmali = $libpath . '/libsmali.jsonl';
$libinfo  = $libpath . '/libinfo.jsonl';

if ( !file_exists($libsmali) ) {
  echo "could not find '$libsmali', exiting.\n";
  exit(5);
} elseif ( !file_exists($libinfo) ) {
  echo "could not find '$libinfo', exiting.\n";
  exit(5);
}


// =======================================================[ Gather Details ]===
//--=[ load library information ]=--
// $libs: collects BY PATH, i.e. a lib with multiple entries in libsmali.jsonl will have multiple entries here, too!
// $map : unique entries by library ID with an array of their paths from libsmali.jsonl (i.e. mapping IDs to their paths)
$libs = []; $map = [];
$locals = file($libsmali); // properties: id, name, type, url
foreach($locals as $line) {
  $lib = json_decode($line);
  $libs[$lib->path] = $lib;
  if ( empty($map[$lib->id]) ) $map[$lib->id] = [];
  $map[$lib->id][] = $lib->path;
}

$locals = file($libinfo); // properties: id, details, anti
foreach($locals as $line) {
  $lib = json_decode($line);
  if ( empty($map[$lib->id]) ) {
    // No libsmali.jsonl entry for this $lib->id, ignore this library
    continue;
  }
  foreach($map[$lib->id] as $path) {
    $libs[$path]->details = $lib->details;
    $libs[$path]->anti = $lib->anti;
    if ( property_exists($lib,'license') ) $libs[$path]->license = $lib->license;
  }
}
//logger("Loaded " .count($libs). " library definitions");


//--=[ Gather Stats ]=--
$types = [];
$licenses = ['Unknown'=>0, 'None'=>0]; $unlic = [];
$antis    = [];
$detnum   = 0; $details = [];
foreach ($map as $id=>$paths) { // walk $map as that has the "unique list by ID" ($lib is "by path")
  $lib = $libs[$paths[0]];
  if ( empty($lib->type) ) continue; // dummy entry (template)

  // types
  if ( isset($types[$lib->type]) ) ++$types[$lib->type];
  else                               $types[$lib->type] = 1;

  // antifeatures
  if ( !empty($lib->anti) ) foreach ($lib->anti as $anti) {
    if ( !isset($antis[$anti]) ) $antis[$anti] = 1;
    else ++$antis[$anti];
  }

  // licenses
  if ( !property_exists($lib,'license') ) { ++$licenses['Unknown']; $unlic[$lib->id] = $lib; }
  elseif ( empty($lib->license) )           ++$licenses['None'];
  elseif ( !isset($licenses[$lib->license]) ) $licenses[$lib->license] = 1;
  else                                      ++$licenses[$lib->license];

  // details
  if ( empty($lib->details) ) {
    ++$detnum;
    $details[$lib->id] = $lib;
  }
}
arsort($types);
arsort($antis);
arsort($licenses);


// =========================================[ Generate & Output Statistics ]===
//--=[ General ]=--
echo "# Library statistics\n";
echo "These are statistics on the library details collected so far. This does not only give you an idea of what details are known – but also about details still missing. Which is where you can help: filling the gaps! So if you know any of those details, please report them (with a reference to confirm them).\n\n";

echo "## General library stats\n";
echo "* number of libraries recorded: " . count($map) ."\n";
echo "* number of library types: " . count($types) . "\n";
echo "* number of AntiFeatures: " . count($antis) . "\n";
echo "\n";

//--=[ Types ]=--
echo "## Library types\n";
echo "| Library type | # libs |\n";
echo "| :----------- | -----: |\n";
foreach ($types as $var => $val) echo "| $var | $val |\n";
echo "\n";

//--=[ AntiFeatures ]=--
echo "## AntiFeatures\n";
echo "| AntiFeature | # libs |\n";
echo "| :---------- | -----: |\n";
foreach ($antis as $var => $val) echo "| $var | $val |\n";
echo "\n";


//--=[ Licenses ]=--
echo "## License Statistics\n";
echo "* Libraries without licenses: " . $licenses['None'] . "\n";
echo "* Libraries with no known license: " . $licenses['Unknown'] ."\n\n";
echo "Top-5 most used licenses:\n\n";
echo "| License | # libs |\n";
echo "| :------ | ---: |\n";
$i = 0; foreach ($licenses as $var => $val) {
  if ( $var == 'None' || $var == 'Unknown' ) continue;
  echo "| $var | $val |\n";
  if ( $i==4) break;
  ++$i;
}
echo "\n";


//--=[ Details ]=--
echo "## Libraries with missing details\n";
echo "* libraries with missing description: $detnum\n";
echo "* libraries with missing/no license: " . $licenses['Unknown'] . " / " . $licenses['None'] . "\n\n";

echo "### Missing descriptions\n";
foreach ($details as $det) {
  if ( empty($det->url) ) echo "* " . $det->name . " (`" . $det->id . "`)\n";
  else echo "* [" . $det->name . "](" . $det->url . ") (`" . $det->id . "`)\n";
}
echo "\n";

echo "### License unknown\n";
foreach ($unlic as $det) {
  if ( empty($det->url) ) echo "* " . $det->name . " (`" . $det->id . "`)\n";
  else echo "* [" . $det->name . "](" . $det->url . ") (`" . $det->id . "`)\n";
}
echo "\n";

?>