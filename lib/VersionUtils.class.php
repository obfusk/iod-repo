<?php

/** Utils class containing the version comparison logic.
 * @class VersionUtils
 * @author Patrick Goldinger
 */
class VersionUtils {
  private const VERSION_NAME_COMPONENTS_REGEX = "/(\d+|alpha|beta|pre|rc|(?<![a-z])[abpr](?![a-z]))/i";

  // This class should not have multiple instances
  private function __construct() {}

  private static function versionComponentToInt($component) {
    $component = strtolower($component);
    switch ($component) {
      case "alpha":
      case "a":
        return -4;
      case "beta":
      case "b":
        return -3;
      case "pre":
      case "p":
        return -2;
      case "rc":
      case "r":
        return -1;
      default:
        return (int) $component;
    }
  }

  /** Compare versions
   * @class VersionUtils
   * @method compareVersionNames
   * @param string oldVer   version we already have
   * @param string newVer   version available remotely
   * @return int update     1 if newver > oldver, 0 otherwise
   */
  static function compareVersionNames($oldVer, $newVer) {
    if (strlen($newVer) == 0) {
      return 0; // Either there is no new version, or we cannot find it
    }

    // Parse the components out of both version names
    preg_match_all(self::VERSION_NAME_COMPONENTS_REGEX, $oldVer, $oldVerNums, PREG_PATTERN_ORDER);
    preg_match_all(self::VERSION_NAME_COMPONENTS_REGEX, $newVer, $newVerNums, PREG_PATTERN_ORDER);

    // Map them to a definite integer for comparison
    $oldVerNums = array_map("VersionUtils::versionComponentToInt", $oldVerNums[0]);
    $newVerNums = array_map("VersionUtils::versionComponentToInt", $newVerNums[0]);

    $oldLen = count($oldVerNums);
    $newLen = count($newVerNums);
    $numsLen = max($oldLen, $newLen);
    for ($i = 0; $i < $numsLen; $i++) {
      $old = $oldLen > $i ? $oldVerNums[$i] : 0;
      $new = $newLen > $i ? $newVerNums[$i] : 0;
      if ($old > $new) {
        return 0;
      } else if ($new > $old) {
        return 1;
      }
    }

    // By default, we return 0 because at this point either the algorithm failed or the version is equal
    return 0;
  }
}

?>
