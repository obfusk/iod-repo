This project is about the F-Droid compatible repo running at https://apt.izzysoft.de/fdroid/

The main purpose of this project is to enable you to actively contribute to the repo (e.g. by
suggesting apps that should be added or removed), as well as to reach out for help. Suggestions
for other improvals are of course welcomed, too.

Further, most of the scripts and libraries used at *IzzyOnDroid* are provided here using FOSS
licenses. You can find those (with some documentation) in the [`bin/`](bin/) and [`lib/`](lib/)
directories and use them according to their resp. licenses.


## What is the IzzyOnDroid Repo?
It is an F-Droid style repository for Android apps, provided by [IzzyOnDroid].
Applications in this repository are official binaries built by the original
application developers, taken from their resp. repositories (mostly Github).
You can find a more detailed description [here][1].


## What is the purpose behind it?
Many users are not happy with their devices' ties to Google, so they try to cut
them back as far as possible. Not being bound to Google's Play Store is part of
that – but the choice of apps is quite restricted outside Playstore if you
don't want to replace one „evil“ with another (i.e. switching to another
„walled garden“ like the Amazon Store) and still have the comfort of updates.
The *IzzyOnDroid* repo wants to increase the number of apps available to this
group.


## Why another repository – hasn't F-Droid its own?
Indeed it has, and a good one at that. But they also have some [inclusion
criteria][2] many apps cannot meet, or are not yet ready to meet
(most common examples include „blobs“ or dependencies on proprietary
frameworks). That's where the *IzzyOnDroid* repo jumps in: Most of those apps
are acceptable here, as the requirements are different. So while e.g. preparing to
meet criteria from „over there“, an app can be (temporarily or even permanently)
hosted here. Other apps also have their permanent place at *IzzyOnDroid* because
they won't ever get rid of those dependencies – for example if those are a central
part of the app's functionality – or their authors simply feel more home here.


## How can I get my favorite app listed there?
This is what this project here at GitLab was set up for: use the issues to
propose new apps. Of course, first make sure the app you wish to add isn't
already there. Then, also check it meets the requirements below.


## What are the requirements an app must meet to be included with the repo?
The app …

* must be free (as in „free beer“ **and** as in „free speech“) and Open Source (see e.g. [Four Freedoms](https://en.wikipedia.org/wiki/The_Free_Software_Definition#The_Four_Essential_Freedoms_of_Free_Software) for what this means). This includes using a libre license, approved by OSI/FSF (see [SPDX](https://spdx.org/licenses/)).
* must be targeted at end users (so no libraries, proof-of-concept demos, etc.)
* must have its code freely accessible, preferably at [Github], [GitLab], [Codeberg] or a similar platform. That code repository should have a proper description so it gets clear what it is (if it's not, you must provide these details)
* preferably has no proprietary components. While some of them might be tolerable, trackers (e.g. ads, analytics) are bearable at best but only if there are no more than two such modules.
* must not download additional executable binary files (e.g. addons, auto-updates, etc.) without explicit user consent. Consent means it needs to be opt-in (it must not be harder to decline than to accept or presented in a way users are likely to press accept without reading) and structured in a way that clearly explains to users that they’re choosing to bypass the checks performed in this repo if they activate it.
* must have its `*.apk` files signed by a release key, and not carry the flags `android:debuggable` or `android:testOnly`. Rare exceptions are possible e.g. when the APKs provided here are explicitly for testing (with the stable releases at [F-Droid] or in this repo using a different `applicationId`). This then needs to be made clear with the app description.
* must have its `.apk` files available from the project. Currently, *IzzyOnDroid* can work with files
    * attached to GitLab `tags/`
    * attached to Github `releases/`
    * attached to Gitea/Forgeio `releases/` (e.g. at [Codeberg])
    * uploaded inside a Github/GitLab repository as „part of the code“
    * fixed URLs (like `https://example.com/app-latest.apk`) which can be redirects, but the web server must provide a `Last Modified` header. This would for example also work with [Sourceforge] using `https://sourceforge.net/projects/<projectName>/files/latest/download` if maintainers do manually mark the corresponding APK file to be the default „latest“ for all platforms, where it not for Cloudflare (which injects their Captcha every now and then).
* if the app processes sensitive data (e.g. health data, passwords), is intended to improve security/privacy, has root permissions, or is targeted at children, it must have **no trackers at all**.
* the `android:usesCleartextTraffic` flag should be avoided wherever possible and only be set were absolutely necessary (e.g. with a media player intended to access resources in the home network it is acceptable).

Running on private resources (no funding), *IzzyOnDroid* usually reserves up
to 30 megabytes per app (exceptions are being made for some larger apps, so this is
considered as rule-of-thumb). That's at the same time the upper size limit for
single `.apk` files. If multiple files can fit in this limit, the repo holds up
to 3 versions. Also, certain categories of apps are not accepted – e.g. games.
Exceptions are made, but not often.


## Are there any app categories which are not acceptable to this repo?
* Games are unlikely to be accepted (though exceptions are possible, e.g. for educational games)
* Apps promoting violence, hate, harassment, racism and similar topics will definitely be rejected.
* „Explicit content“ is not welcome here – mainly for the reason that all age groups incl. „minors“ should have access to this repo.
* Apps which are loaded with trackers (Analytics, Ads, etc.) are no good, as they pose dangers to the user's privacy.
* Also for the reason of tracking, apps for Facebook, WhatsApp & Co won't be accepted.

This list is not complete (I might add to it if need arises), but should give you a raw idea.


## If my app is available here, do you have a badge I can use to link to it?
Yes, indeed I have – for the purpose of linking to my repo, you can use [this PNG](assets/IzzyOnDroid.png), or one of the other graphics available in the [`assets/` directory](assets/) (see there for details on usage and credits):

<center><img src="assets/IzzyOnDroid.png" width="170"></center>

You can also use Shields.io to show which version of your app is available at IzzyOnDroid. Details on this
can be found [in the wiki](https://gitlab.com/IzzyOnDroid/repo/-/wikis/API). This is how it would look like:

<center><img src="https://img.shields.io/endpoint?url=https://apt.izzysoft.de/fdroid/api/v1/shield/com.mirfatif.permissionmanagerx&label=IzzyOnDroid&cacheSeconds=86400"></center>


## Why is $someApp only available as $someArch?
Some apps are only available with e.g. their `arm64` or `armeabi` variants, though their developers also serve other architectures. This is due to size limitations (see inclusion criteria above) and happens when the „fat build“ (i.e. the one holding multiple architectures in a single APK) would not fit the per-app size limit. Usually, `armeabi` is chosen here, as the repo strives to help keeping old devices useful as long as possible (keyword: sustainability) and most 64bit devices still support 32bit APKs (exceptions are still very few) – while the opposite can not be said (a 32bit device naturally cannot support 64bit APKs). In few cases, the `arm64` is here instead – usually if there are enough other alternatives available.

If you think the other arch of an app should be here instead of the one that is here, please clarify that with the app developers first. I usually follow their decision on this, and won't switch architectures without their agreement. And before you ask: architectures other than `armeabi` and `arm64` won't be supported here except as part of a „fat build“ including ARM as well.


## Are apps removed from the repo – and when does that happen?
This indeed is the case, and usually happens when the inclusion criteria (see above) are no longer met. So this is especially the case when an app…

* can no longer count as „free/libre“ (e.g. the license changed, or too many non-free dependencies have been added)
* started downloading additional binaries without the explicit and informed consent of the user (e.g. by integrating a self-updater)
* is reported (with evidence) for malicious behavior
* is no longer maintained and reported to be no longer working
* exceeds the alotted space with no means of remediation (a possible remediation often is switching to a per-ABI build, see above „`$somearch`“)

Note that becoming available in another F-Droid Repo (e.g. at [F-Droid]) no longer means it will be „automatically removed“ here. If the size of its APK files stays well inside the limits outlined above, it will usually be kept.


## I'm just a simple, not tech-savy user, how can I support you?
True, there's much time I spend on my open source projects, and server costs have
to be considered as well. So if you're just a happy user looking how you can give
some „backing“ – you're very welcome to do so spending some mBTC to my address
[1K7i1VJYjRgjdVzaMfK2XRxLsyhjSFXPnC](bitcoin:1K7i1VJYjRgjdVzaMfK2XRxLsyhjSFXPnC).
For other alternatives like SEPA payment, please find detailed [explanations at
IzzyOnDroid][3].

Apart from that, support is always welcomed with

* improving app descriptions
* providing missing screenshots
* reporting missing/wrong URLs (e.g. if an app's repo moved, or a website/changelog was added)
* finding and reporting good apps matching above criteria so they can be added
* reporting „violating” apps as well as such that no longer work so they can be removed
* filling [gaps in the library details](LibraryStats.md)

If you’re going on a hunt to report a bulk, please open one issue per app so it
will be easier to process. But even if you by accident stumble on a little thing
to be improved, your report will be welcome!


[IzzyOnDroid]: https://android.izzysoft.de/
[Github]: https://github.com/
[GitLab]: https://gitlab.com/
[Codeberg]: https://codeberg.org/
[Sourceforge]: https://sourceforge.net/
[F-Droid]: https://f-droid.org/
[1]: https://apt.izzysoft.de/fdroid/index/info
[2]: https://f-droid.org/wiki/page/Inclusion_Policy "F-Droid Inclusion Policy"
[3]: https://android.izzysoft.de/help?topic=support_us "Say thanks to IzzyOnDroid"
